import './Menu.scss';
import '../../scss/_fonts.scss';
import { Link } from "react-router-dom";

function Menu() {
	return (
		<nav>
			<ul className='hr'>
				<li>
					<Link to={'/'}>Домашняя страница</Link>
				</li>
				<li>
					<Link to={'/login'}>Страница входа</Link>
				</li>
				<li>
					<Link to={'/register'}>Страница регистрации</Link>
				</li>
			</ul>
		</nav>
	);
}

export default Menu;
