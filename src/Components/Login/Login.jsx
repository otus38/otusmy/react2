import React, { useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { setUser } from "../../reducers/active-reducer";
import { TextField, FormControl, Button } from "@mui/material";
import { Link } from "react-router-dom"

const Login = () => {
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [emailError, setEmailError] = useState(false)
	const [passwordError, setPasswordError] = useState(false)
	const users = useSelector((store) => store.users.reginfo);
	const dispath = useDispatch();

	const handleSubmit = (event) => {
		event.preventDefault()

		setEmailError(email == '')
		setPasswordError(password == '')

		if (email && password) {
			// console.log("Массив users ", users);
			let hasUser = users.find((item) =>
				item.email === email && item.password === password);
			// console.log("Существует с email ", hasUser);
			if (hasUser) { alert("Регистрация прошла успешно") }
			else {
				alert("Ошибка входа !");
				hasUser = {};
			};
			dispath(setUser(hasUser));
		}

	}

	return (
		<React.Fragment>
			<form autoComplete="off" onSubmit={handleSubmit}>
				<h2>Форма логирования</h2>
				<TextField
					label="Email"
					onChange={e => setEmail(e.target.value)}
					required
					variant="outlined"
					color="secondary"
					type="email"
					sx={{ mb: 3 }}
					fullWidth
					value={email}
					error={emailError}
				/>
				<TextField
					label="Password"
					onChange={e => setPassword(e.target.value)}
					required
					variant="outlined"
					color="secondary"
					type="password"
					value={password}
					error={passwordError}
					fullWidth
					sx={{ mb: 3 }}
				/>
				<Button variant="outlined" color="secondary" type="submit">Войти</Button>

			</form>
			<small>Нужна регистрация? <Link to="/register">Зарегистрируйтесь здесь</Link></small>
		</React.Fragment>
	);
}

export default Login;