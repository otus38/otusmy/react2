import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { addUser } from "../../reducers/user-reducer";
import { setUser } from "../../reducers/active-reducer";
import { saveStorage } from "../../utils/localStoreUtil"
import { TextField, Button, Container, Stack } from '@mui/material';
import { Link } from "react-router-dom"


const RegisterForm = () => {
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [dateOfBirth, setDateOfBirth] = useState('')
	const [password, setPassword] = useState('')
	const users = useSelector((store) => store.users.reginfo);
	const dispath = useDispatch();

	function handleSubmit(event) {
		event.preventDefault();
		const newUser = {
			firstName: firstName, lastName: lastName, email: email,
			dateOfBirth: dateOfBirth, password: password
		};
		const hasUser = users.find((item) => item.email === email);
		if (hasUser) {
			alert("Пользователь с таким email уже существует");
			return
		}
		alert("Регистрация прошла успешно!");
		dispath(addUser(newUser));
		dispath(setUser(newUser));
		saveStorage("users", [...users, newUser]);
	}

	return (
		<React.Fragment>
			<h2>Форма регистрации</h2>
			<form onSubmit={handleSubmit} action={<Link to="/login" />}>
				<Stack spacing={2} direction="row" sx={{ marginBottom: 4 }}>
					<TextField
						type="text"
						variant='outlined'
						color='secondary'
						label="Имя"
						onChange={e => setFirstName(e.target.value)}
						value={firstName}
						fullWidth
						required
					/>
					<TextField
						type="text"
						variant='outlined'
						color='secondary'
						label="Фамилия"
						onChange={e => setLastName(e.target.value)}
						value={lastName}
						fullWidth
						required
					/>
				</Stack>
				<TextField
					type="email"
					variant='outlined'
					color='secondary'
					label="Email"
					onChange={e => setEmail(e.target.value)}
					value={email}
					fullWidth
					required
					sx={{ mb: 4 }}
				/>
				<TextField
					type="password"
					variant='outlined'
					color='secondary'
					label="Password"
					onChange={e => setPassword(e.target.value)}
					value={password}
					required
					fullWidth
					sx={{ mb: 4 }}
				/>
				<TextField
					type="date"
					variant='outlined'
					color='secondary'
					label="Дата рождения"
					onChange={e => setDateOfBirth(e.target.value)}
					value={dateOfBirth}
					fullWidth
					required
					sx={{ mb: 4 }}
				/>
				<Button variant="outlined" color="secondary" type="submit">Зарегистрироваться</Button>
			</form>
			<small>Уже зарегистрирован? <Link to="/login">Войди здесь</Link></small>

		</React.Fragment>
	)
}

export default RegisterForm;