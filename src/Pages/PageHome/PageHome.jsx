import './PageHome.scss';
import '../../scss/_container.scss';
import '../../scss/_fonts.scss';

function PageHome() {
	return (
		<>
			<div className="container container_items_strech" >
				<div id="begin"></div>
				<main className="home">
					<div className="home__item font__h1">Приветствуем Вас на главной странице</div>
				</main>
			</div>
		</>
	);
}

export default PageHome;
