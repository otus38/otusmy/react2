import './PageNotFound.scss';
import '../../scss/_container.scss';
import '../../scss/_fonts.scss';
import { Link } from "react-router-dom";

function PageNotFound() {
	return (
		<>
			<div className="container container_items_strech" >
				<div id="begin"></div>
				<main className="notfound">
					<div className="notfound__item notfound__item_font_big">404</div>
					<h1 className="notfound__item">Мы не можем найти страницу, которую вы ищете.</h1>
					<div className="notfound__item">
						<p>Страница, которую вы запросили, не найдена в базе данных.</p>
						<p>Скорее всего вы попали на ошибочную ссылку или опечатались при вводе URL</p>
					</div>
					<div className="notfound__item">
						<Link to={'/'}>Перейти на главную страницу</Link>
					</div>
				</main>
			</div>
		</>
	);
}

export default PageNotFound;
