import React from 'react';
import './App.css';

import './scss/_fonts.scss';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { PageHomeWithMenu, PageNotFoundWithMenu, LoginWithMenu, RegisterWithMenu } from './utils/CompWithMenu'
import { Link } from "react-router-dom"

function App() {
  return (
    <div className="container-app font">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<PageHomeWithMenu />} />
          {/* Параметр path указываем по какому адресу будут доступна эта страница */}
          <Route path="/login" element={<LoginWithMenu />} />
          {/* Если человек ввел другой адрес, то показываем страницу 404 */}
          <Route path="/register" element={<RegisterWithMenu />} />
          {/* Если человек ввел другой адрес, то показываем страницу 404 */}
          <Route path="*" element={<PageNotFoundWithMenu />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
