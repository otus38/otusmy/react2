import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
	name: "active",
	// Начальное состояние хранилища, пользователей нет
	initialState: {
		actUser: {},
	},
	// Все доступные методы
	reducers: {
		// Добавить пользователя, первый параметр это текущее состояние
		// А второй параметр имеет данные для действия
		setUser: (prevState, action) => {
			const user = action.payload;
			// console.log("Act в red ", user);
			return {
				...prevState,
				// Внутри action.payload информация о пользователе
				// Устанавливаем пользователя
				actUser: user
			};
		},
	},
});
// Экспортируем наружу все действия
export const { setUser } = userSlice.actions

// И сам редуктор тоже
export default userSlice.reducer;
