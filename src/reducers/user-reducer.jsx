import { createSlice } from "@reduxjs/toolkit";
import { initStored } from '../utils/localStoreUtil'

export const userSlice = createSlice({
	name: "user",
	// Начальное состояние хранилища, пользователей нет
	initialState: {
		reginfo: initStored("users"),
	},
	// Все доступные методы
	reducers: {
		// Добавить пользователя, первый параметр это текущее состояние
		// А второй параметр имеет данные для действия
		addUser: (prevState, action) => {
			const user = action.payload;
			const hasInUsers = prevState.reginfo.some(
				(prevUser) => prevUser.email === user.email);
			if (hasInUsers) return prevState;
			return {
				...prevState,
				// Внутри action.payload информация о добавленном пользователе
				// Возвращаем новый массив пользователей вместе с добавленным
				users: [...prevState.reginfo, action.payload],
			};
		},
		removeUser: (prevState, action) => {
			const user = action.payload;
			return {
				...prevState,
				users: prevState.reginfo.filter((prevUser) => {
					return prevUser.email !== user.email;
				})
			};
		}
	},
});
// Экспортируем наружу все действия
export const { addUser, removeUser } = userSlice.actions

// И сам редуктор тоже
export default userSlice.reducer;
