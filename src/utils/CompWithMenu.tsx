import Menu from './../Components/Menu/Menu'
import PageHome from "./../Pages/PageHome/PageHome";
import PageNotFound from "./../Pages/PageNotFound/PageNotFound";
import Login from "./../Components/Login/Login";
import Register from "./../Components/Register/Register";

export default function CompWithMenu(ToRender: any) {
	return function <T>(props: T) {
		return (
			<>
				<Menu />
				<ToRender {...props} />;
			</>
		)
	};
}

export const PageHomeWithMenu = CompWithMenu(PageHome);
export const PageNotFoundWithMenu = CompWithMenu(PageNotFound);
export const LoginWithMenu = CompWithMenu(Login);
export const RegisterWithMenu = CompWithMenu(Register);


